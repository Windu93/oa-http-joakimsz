FROM ubuntu

RUN apt update && apt upgrade -y
RUN apt install -y build-essential cmake

COPY . home/src/academy-practice
WORKDIR home/src/academy-practice

RUN chmod +x ./compile.sh 

EXPOSE 3000

ENTRYPOINT [ "./compile.sh" ] 
